package calculator1;

public class ScientificCalc {

    public static double memory;

    public static double getMemory() {
        return memory;
    }

    public static void setMemory(double n) {
        memory = n;
    }

    public static double add(double a, double b) {

        return a + b;
    }

    public static double sub(double a, double b) {

        return a - b;
    }

    public static double mul(double a, double b) {

        return a * b;
    }

    public static double div(double a, double b) {

        return a / b;
    }

    public static int mod(double a, double b) {

        return (int) (a % b);
    }

    public static double pow(double a, double b) {

        return Math.pow(a, b);
    }

    public static double sin(double a) {
        double radius = Math.toRadians(a);
        return Math.sin(radius);
    }

    public static double cos(double a) {
        double radius = Math.toRadians(a);
        return Math.cos(radius);
    }

    public static double tan(double a) {
        double radius = Math.toRadians(a);
        return Math.tan(radius);
    }

    public static double root(double a, double b) {
        b = 1 / b;
        return Math.pow(a, b);
    }

    public static double clearMemory() {
        return memory = 0;
    }
}


