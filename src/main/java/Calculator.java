import calculator1.ScientificCalc;
import calculator1.StandardCalc;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("1. To open Standard Calculator \n2. To open Scientific Calculator \n");
        char mode = in.next().charAt(0);
        if (mode == '1') {

            System.out.println("Enter 2 values: ");
            Double a = in.nextDouble();
            Double b = in.nextDouble();
            System.out.println("enter the operator");
            String ch = in.next();

            switch (ch) {
                case "+":
                    double add1 = StandardCalc.add(a, b);
                    System.out.println("Addition:" + add1);
                    break;
                case "-":
                    double sub1 = StandardCalc.sub(a, b);
                    System.out.println("Subtraction:" + sub1);
                    break;
                case "*":
                    double mul1 = StandardCalc.mul(a, b);
                    System.out.println("Multiplication:" + mul1);
                    break;
                case "/":
                    double div1 = StandardCalc.div(a, b);
                    System.out.println("Division:" + div1);
                    break;
                case "%":
                    double mod1 = StandardCalc.mod(a, b);
                    System.out.println("Modulus:" + mod1);
                    break;
                case "c":
                    int mem = (int) StandardCalc.clearMemory();
                    System.out.println("clear memory" + mem);
                    break;
                default:
                    System.out.println("Invalid Operation: " + ch);
            }

        } else if (mode == '2') {


            System.out.println("enter the operator");
            String ch = in.next();
            if ((ch.equals("+")) || (ch.equals("-")) || (ch.equals("*")) || (ch.equals("/")) || (ch.equals("%")) || (ch.equals("pow")) || (ch.equals("root")) || (ch.equals("c"))) {
                System.out.println("Enter 2 values: ");
                Double a = in.nextDouble();
                Double b = in.nextDouble();

                switch (ch) {
                    case "+":
                        double add1 = ScientificCalc.add(a, b);
                        ScientificCalc.setMemory(add1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Addition:" + add1);
                        break;
                    case "-":
                        double sub1 = ScientificCalc.sub(a, b);
                        ScientificCalc.setMemory(sub1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Subtraction:" + sub1);
                        break;
                    case "*":
                        double mul1 = ScientificCalc.mul(a, b);
                        ScientificCalc.setMemory(mul1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Multiplication:" + mul1);
                        break;
                    case "/":
                        double div1 = ScientificCalc.div(a, b);
                        ScientificCalc.setMemory(div1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Division:" + div1);
                        break;
                    case "%":
                        int mod1 = ScientificCalc.mod(a, b);
                        ScientificCalc.setMemory(mod1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Modulus:" + mod1);
                        break;
                    case "pow":
                        double pow1 = ScientificCalc.pow(a, b);
                        ScientificCalc.setMemory(pow1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Power:" + pow1);
                        break;
                    case "root":
                        double rt1 = ScientificCalc.root(a, b);
                        ScientificCalc.setMemory(rt1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Root:" + rt1);
                        break;
                    case "c":
                        int mem = (int) ScientificCalc.clearMemory();
                        System.out.println("clear memory" + mem);
                        break;
                    default:
                        throw new IllegalStateException("Invalid operation: " +ch);

                }

            }

            else  if(ch.equals("sin") || ch.equals("cos") || ch.equals("tan") || ch.equals("log")){
                System.out.println("enter the value: ");
                Double a = in.nextDouble();
                switch (ch) {

                    case "sin":
                        double s1 = ScientificCalc.sin(a);
                        ScientificCalc.setMemory(s1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Sin():" + s1);
                        break;
                    case "cos":
                        double c1 = ScientificCalc.cos(a);
                        ScientificCalc.setMemory(c1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("cos():" + c1);
                        break;
                    case "tan":
                        double t1 = ScientificCalc.tan(a);
                        ScientificCalc.setMemory(t1);
                        System.out.println(ScientificCalc.getMemory());
                        System.out.println("Tan():" + t1);
                        break;



                }
            }
            else {
                System.out.println("Invalid Operation: " + ch);
            }
        }
    }
}








